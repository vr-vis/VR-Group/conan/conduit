from conans import ConanFile, CMake

class ConduitTest(ConanFile):
    name = "ConduitTest"
    license = "Apache License, Version 2.0"
    description = """A test for the conan recipe for conduit"""
    settings = "os", "compiler", "build_type", "arch"

    requires = "conduit/0.3.1@RWTH-VR/thirdparty"
    generators = "cmake"

    def configure(self):
        self.options["conduit"].shared = False
