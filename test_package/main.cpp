#include "conduit/conduit_node.hpp"

int main() {
  conduit::Node a;
  a["b/c"] = 3.1415;
  std::cout << a["b/c"].as_double();
}