from conans import ConanFile, CMake, tools
from conans.tools import download, unzip
import os


class ConduitConan(ConanFile):
    name = "conduit"
    version = "0.3.1"
    license = "BSD"
    url = "https://bintray.com/rwth-vr/"
    description = "Conan package for Conduit"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=True"
    generators = "cmake"

    def source(self):
        self.run("git clone https://github.com/LLNL/conduit.git")
        self.run("git checkout tags/v%s" % self.version, cwd="conduit")
        self.run("git submodule init", cwd="conduit")
        self.run("git submodule update", cwd="conduit")

    def build(self):
        cmake = CMake(self)
        cmake_flags = "-DENABLE_TESTS=OFF -DENABLE_DOCS=OFF -DCMAKE_SKIP_RPATH=ON"

        if self.settings.compiler == "Visual Studio":
            if self.settings.compiler.version == 15:
                compiler = "Visual Studio 15 2017"
            else:
                self.output.error("Invalid compiler or compiler version")

            if self.settings.arch == "x86_64":
                compiler = "%s %s" % (compiler, "Win64")
                self.output.info("64-bit build")
            elif self.settings.arch == "x86":
                self.output.info("32-bit build")
            cmake_flags = "%s -G \"%s\"" % (cmake_flags, compiler)
        else:
            if self.settings.build_type == "Release":
                cmake_flags = "%s %s" % (
                    cmake_flags, "-DCMAKE_BUILD_TYPE=Release")
            elif self.settings.build_type == "Debug":
                cmake_flags = "%s %s" % (
                    cmake_flags, "-DCMAKE_BUILD_TYPE=Debug")
            elif self.settings.build_type == "RelWithDebInfo":
                cmake_flags = "%s %s" % (
                    cmake_flags, "-DCMAKE_BUILD_TYPE=RelWithDebInfo")
            else:
                self.output.error("Invalid build_type")

        if self.options.shared:
            cmake_flags = "%s %s" % (cmake_flags, "-DBUILD_SHARED_LIBS=ON")
        else:
            cmake_flags = "%s %s" % (cmake_flags, "-DBUILD_SHARED_LIBS=OFF")

        print("cmake %s conduit/src" % cmake_flags)
        self.run("cmake %s conduit/src" % cmake_flags)

        cmake_build_flags = ""
        if self.settings.compiler == "Visual Studio":
            if self.settings.build_type == "Release":
                cmake_build_flags = "%s %s" % (
                    cmake_build_flags, "--config Release")
            elif self.settings.build_type == "Debug":
                cmake_build_flags = "%s %s" % (
                    cmake_build_flags, "--config Debug")
            elif self.settings.build_type == "RelWithDebInfo":
                cmake_build_flags = "%s %s" % (
                    cmake_build_flags, "--config RelWithDebInfo")
            else:
                self.output.error("Invalid build_type")
        self.run("cmake --build . %s" % cmake_build_flags)

    def package(self):
        self.copy("*.hpp", dst="include", src="conduit/src/libs")
        self.copy("*.h", dst="include", src="conduit/src/libs")
        self.copy("*.h", dst="include", src="libs")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so*", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False, symlinks=True)

    def package_info(self):
        self.cpp_info.libs = ["conduit"]
